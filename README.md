# Desafio Frontend Totvs

> Iron Man Comics

## Build Setup

``` bash
# install dependencies
npm install

# build for production with minification
npm run build

# serve at localhost:8080
npm run serve

# lint all *.js and *.vue files
npm run lint