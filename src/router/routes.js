import Home from '../components/Home/Index.vue'
import Comic from '../components/Comic/Index.vue'

export default [
  {
    path: '/home',
    alias: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/comic/:id',
    name: 'Comic',
    component: Comic
  }
]
