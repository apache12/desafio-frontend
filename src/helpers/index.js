module.exports = {
  getDate (dates) {
    const curr = dates.find(item => {
      return item.type === 'focDate'
    })

    return curr.date
  }
}
