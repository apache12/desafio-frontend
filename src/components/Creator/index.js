export default {
  name: 'creator',
  props: {
    data: {
      type: Object,
      required: true
    }
  }
}
