// Components
import Creator from '../Creator/Index.vue'

// Attrs
import data from './attrs/data'
import created from './attrs/created'
import methods from './attrs/methods'

// Mixins
import MXHelper from '../../mixins/Helper'

export default {
  name: 'comic',
  mixins: [ MXHelper ],
  components: {
    Creator
  },
  data,
  created,
  methods
}
