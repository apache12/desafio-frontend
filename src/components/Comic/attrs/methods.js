export default {
  getComic (comicId) {
    return new Promise((resolve, reject) => {
      this.$store.dispatch('GET_COMIC', comicId)
      .then(comic => {
        this.comic = comic
        resolve()
      })
      .catch(err => {
        reject(err)
      })
    })
  },

  getCreators (comicId) {
    return new Promise((resolve, reject) => {
      this.$store.dispatch('GET_CREATORS', comicId)
      .then(creators => {
        this.creators = creators
      })
      .catch(err => {
        reject(err)
      })
    })
  }
}
