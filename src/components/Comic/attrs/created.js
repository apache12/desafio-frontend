export default function () {
  // this.$store.commit('LOADER_IS_ACTIVE', true)
  const comicId = this.$route.params.id
  const comic = this.$store.getters['comics'].find(item => item.id == comicId) // eslint-disable-line eqeqeq

  if (comic) {
    this.comic = comic

    this.getCreators(comicId)
    .then(creators => {
      this.creators = creators
      this.$store.commit('LOADER_IS_ACTIVE', false)
    })
    .catch(() => {
      this.$store.commit('LOADER_IS_ACTIVE', false)
    })
  }
  else {
    this.getComic(comicId)
    .then(() => {
      this.getCreators(comicId)
      .then(creators => {
        this.creators = creators
        this.$store.commit('LOADER_IS_ACTIVE', false)
      })
      .catch(() => {
        this.$store.commit('LOADER_IS_ACTIVE', false)
      })
    })
    .catch(() => {
      this.$store.commit('LOADER_IS_ACTIVE', false)
    })
  }
}
