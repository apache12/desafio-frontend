export default {
  handleClick (id) {
    this.$router.push(`/comic/${id}`)
  },
  startScrollInfinite () {
    const discount = window.isMobile ? 570 : 970

    window.addEventListener('scroll', e => {
      if (window.scrollY > (document.body.scrollHeight - discount)) {
        this.loadMore()
      }
    })
  },
  loadMore () {
    if (!this.isLoadMoreBlocked) {
      this.isLoadMoreBlocked = true

      this.$store.dispatch('GET_COMICS')
      .then(() => {
        this.isLoadMoreBlocked = false
      })
      .catch(() => {
        this.isLoadMoreBlocked = false
      })
    }
  },
  validateResult () {
    this.showEmptyResult = this.$store.getters['comics'].length < 1
  }
}
