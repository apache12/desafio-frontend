export default function () {
  if (this.$store.getters['comics'].length < 1) {
    this.$store.commit('LOADER_IS_ACTIVE', true)

    this.$store.dispatch('GET_COMICS')
    .then(() => {
      this.validateResult()
      this.startScrollInfinite()
      this.$store.commit('LOADER_IS_ACTIVE', false)
    })
    .catch(() => {
      this.$store.commit('LOADER_IS_ACTIVE', false)
    })
  }
}
