// Vuex
import { mapGetters } from 'vuex'

// Components
import Card from '../Card/Index.vue'

// Attrs
import data from './attrs/data'
import created from './attrs/created'
import methods from './attrs/methods'

export default {
  name: 'home',
  components: {
    Card
  },
  data,
  computed: mapGetters({
    comics: 'comics'
  }),
  created,
  methods
}
