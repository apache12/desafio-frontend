// Attrs
import props from './attrs/props'

// Mixins
import MXHelper from '../../mixins/Helper'

export default {
  name: 'card',
  mixins: [ MXHelper ],
  props
}
