export default {
  data: {
    type: Object,
    required: true
  },
  handleClick: {
    type: Function,
    required: true
  }
}
