import moment from 'moment'
import formatCurrency from 'format-currency'
const formatCurrencyOptions = {
  format: '%s %v', symbol: 'R$', locale: 'pt-BR'
}

const Helper = {
  filters: {
    date (timestamp = '') {
      return moment(timestamp).format('DD/MM/YYYY')
    },
    ellipsis (string = '') {
      const clamp = '...'
      const length = 210

      return string.length > length ? string.slice(0, length) + clamp : string
    },
    formatPrice (price) {
      return formatCurrency(price, formatCurrencyOptions)
    }
  }
}

export default Helper
