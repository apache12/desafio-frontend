export default {
  comics: state => state.comics,
  creators: state => state.creators,
  offset: state => state.offset,
  loaderIsActive: state => state.loaderIsActive
}
