export default {
  COMICS (state, comics = []) {
    state.comics.push(...comics)
  },

  CREATORS (state, creators = []) {
    state.creators = creators
  },

  LOADER_IS_ACTIVE (state, loaderIsActive = false) {
    state.loaderIsActive = loaderIsActive
  },

  OFFSET (state, offset) {
    state.offset = offset
  }
}
