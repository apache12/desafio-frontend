import Vue from 'vue'
import Vuex from 'vuex'

import api from '../plugins/api'
import apiOptions from '../config/apiOptions'

Vue.use(Vuex)
Vue.use(api, apiOptions)

// store
import state from './state'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
  strict: false
})
