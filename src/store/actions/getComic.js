import Vue from 'vue'
import { getDate } from '../../helpers'

export default function ({ getters, commit }, comicId) {
  return new Promise((resolve, reject) => {
    Vue.api.comics.get(comicId)
    .then(({ data }) => {
      const [ item ] = data.data.results

      resolve({
        id: item.id,
        title: item.title,
        desc: item.description,
        price: item.prices[0].price,
        src: `${item.thumbnail.path}/landscape_incredible.${item.thumbnail.extension}`,
        date: getDate(item.dates),
        pages: item.pageCount
      })
    })
    .catch(err => console.warn(err))
  })
}
