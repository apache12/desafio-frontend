import GET_COMICS from './getComics'
import GET_CREATORS from './getCreators'
import GET_COMIC from './getComic'

export default {
  GET_COMICS,
  GET_CREATORS,
  GET_COMIC
}
