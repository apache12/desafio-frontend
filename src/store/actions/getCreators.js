import Vue from 'vue'

export default function ({ getters }, comic = '') {
  return new Promise((resolve, reject) => {
    Vue.api.creators.get(comic)
    .then(({ data }) => {
      resolve(data.data.results.map(item => {
        return {
          name: item.fullName,
          src: `${item.thumbnail.path}/standard_medium.${item.thumbnail.extension}`
        }
      }))
    })
    .catch(err => console.warn(err))
  })
}
