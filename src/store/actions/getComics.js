import Vue from 'vue'
import { getDate } from '../../helpers'

export default function ({ getters, commit }) {
  return new Promise((resolve, reject) => {
    const charactersId = '1009368'

    Vue.api.characters.getById(charactersId, getters['offset'])
    .then(({ data }) => {
      commit('OFFSET', data.data.count)
      commit('COMICS', data.data.results.map(item => {
        return {
          id: item.id,
          title: item.title,
          desc: item.description,
          price: item.prices[0].price,
          src: `${item.thumbnail.path}/landscape_incredible.${item.thumbnail.extension}`,
          date: getDate(item.dates),
          pages: item.pageCount
        }
      }))

      resolve()
    })
    .catch(err => reject(err))
  })
}
