import api from './api'

export default {
  install (Vue, options = {}) {
    Vue.api = api(options)

    Vue.prototype.$api = () => {
      return Vue.api
    }
  }
}
