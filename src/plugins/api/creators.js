module.exports = api => {
  let instance = {}

  instance.get = comic => api.get('/creators', {
    comics: comic
  })

  return instance
}
