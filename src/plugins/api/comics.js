module.exports = api => {
  let instance = {}

  instance.get = id => api.get(`/comics/${id}`, {})

  return instance
}
