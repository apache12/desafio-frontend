module.exports = api => {
  let instance = {}

  instance.getById = (id = '', offset = 0) => api.get(`/characters/${id}/comics`, {
    limit: 12,
    offset: offset,
    orderBy: '-focDate'
  })

  return instance
}
