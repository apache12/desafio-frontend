import axios from 'axios'
import md5 from 'md5'

// Sub-modules
import characters from './characters'
import creators from './creators'
import comics from './comics'

module.exports = options => {
  let instance = {}

  instance.host = options.host
  instance.ts = options.ts
  instance.publicKey = options.publicKey
  instance.privateKey = options.privateKey

  instance.basePath = '/v1/public'

  instance.get = (url, data = {}) => {
    let path = instance.host + instance.basePath + url

    return axios.get(path, {
      params: Object.assign(data, {
        ts: instance.ts,
        apikey: instance.publicKey,
        hash: md5(instance.ts + instance.privateKey + instance.publicKey)
      })
    })
  }

  instance.characters = characters(instance)
  instance.creators = creators(instance)
  instance.comics = comics(instance)

  return instance
}
